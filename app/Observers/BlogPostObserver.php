<?php

namespace App\Observers;

use App\Models\BlogPost;
use Carbon\Carbon;
use Str;

class BlogPostObserver
{
	/**
	 * Handle before the blog post "creating" event.
	 *
	 * @param BlogPost $blogPost
	 *
	 * @return void
	 */
	public function creating(BlogPost $blogPost)
	{
		$this->setPublishedAt($blogPost);

		$this->setSlug($blogPost);

		$this->setHtml($blogPost);

		$this->setUser($blogPost);
	}

	/**
	 * Якщо поле "Слаг" пусте, то встановлюємо його з конвертиції заголовка.
	 *
	 * @param BlogPost $blogPost
	 */
	protected function setSlug(BlogPost $blogPost)
	{
		if (empty($blogPost->slug)) {
			$blogPost->slug = Str::slug($blogPost->title);
		}
	}

	/**
	 * Встановлення значення полю "content_html" відносно поля "content_raw".
	 *
	 * @param BlogPost $blogPost
	 */
	protected function setHtml(BlogPost $blogPost)
	{
		if ($blogPost->isDirty('content_raw')) {
			// ToDo: generation markdown -> html
			$blogPost->content_html = $blogPost->content_raw;
		}
	}

	/**
	 * Якщо не вказано значення поля "user_id", то встановлюємо користувача за-замовчуванням.
	 *
	 * @param BlogPost $blogPost
	 */
	protected function setUser(BlogPost $blogPost)
	{
		$blogPost->user_id = auth()->id() ?? BlogPost::UNKNOWN_USER;
	}

	/**
	 * Handle the blog post "created" event.
	 *
	 * @param BlogPost $blogPost
	 *
	 * @return void
	 */
	public function created(BlogPost $blogPost)
	{
		//;
	}

	/**
	 * Handle before the blog post "updating" event.
	 *
	 * @param BlogPost $blogPost
	 *
	 * @return void
	 */
	public function updating(BlogPost $blogPost)
	{
		$this->setPublishedAt($blogPost);

		$this->setSlug($blogPost);
	}

	/**
	 * Якщо дата публікації не встановлена і йде встановлення прапору "Опубліковано",
	 * то встановлюємо дату публікації на дану.
	 *
	 * @param BlogPost $blogPost
	 */
	protected function setPublishedAt(BlogPost $blogPost)
	{
		if (empty($blogPost->published_at) && $blogPost->is_published) {
			$blogPost->published_at = Carbon::now();
		}
	}

	/**
	 * Handle the blog post "updated" event.
	 *
	 * @param BlogPost $blogPost
	 *
	 * @return void
	 */
	public function updated(BlogPost $blogPost)
	{
		//
	}

	public function deleting(BlogPost $blogPost)
	{
//		return false;
	}

	/**
	 * Handle the blog post "deleted" event.
	 *
	 * @param BlogPost  $blogPost
	 *
	 * @return void
	 */
	public function deleted(BlogPost $blogPost)
	{
		//
	}

	/**
	 * Handle the blog post "restored" event.
	 *
	 * @param  BlogPost  $blogPost
	 *
	 * @return void
	 */
	public function restored(BlogPost $blogPost)
	{
		//
	}

	/**
	 * Handle the blog post "force deleted" event.
	 *
	 * @param  BlogPost  $blogPost
	 *
	 * @return void
	 */
	public function forceDeleted(BlogPost $blogPost)
	{
		//
	}
}
