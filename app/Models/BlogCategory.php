<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BlogCategory
 *
 * @package App\Models
 *
 * @property-read BlogCategory $parentCategory
 * @property-read string $parentTitle
 */
class BlogCategory extends Model
{
    use SoftDeletes;

	/**
	 * Id корня
	 */
    const ROOT = 1;

    protected $fillable = [
    	'title',
    	'slug',
    	'parent_id',
    	'description',
    ];

	/**
	 * Отримання батьківської категорії
	 *
	 * @return BelongsTo
	 */
    public function parentCategory(): BelongsTo
	{
		return $this->belongsTo(BlogCategory::class, 'parent_id', 'id');
	}

	/**
	 * Приклад аксесуара (Accessor)
	 *
	 * @url https://laravel.com/docs/5.8/eloquent-mutators
	 *
	 * @return string
	 */
	public function getParentTitleAttribute(): string
	{
		return $this->parentCategory->title
			   ?? ($this->isRoot()
				? 'Корінь'
				: '???');
	}

	/**
	 * Чи являється теперішній обʼєкт корневим
	 *
	 * @return bool
	 */
	public function isRoot(): bool
	{
		return $this->id === BlogCategory::ROOT;
	}

	/**
	 * Приклад аксесуара
	 *
	 * @param string $valueFromObject
	 *
	 * @return string
	 */
	public function getTitleAttribute(string $valueFromObject): string
	{
		return mb_strtoupper($valueFromObject);
	}

	/**
	 * Приклад мутатора
	 *
	 * @param string $valueFromObject
	 */
	public function setTitleAttribute(string $valueFromObject)
	{
		$this->attributes['title'] =  mb_strtolower($valueFromObject);
	}
}
