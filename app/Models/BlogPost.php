<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BlogPost
 *
 * @package App\Models
 *
 * @property BlogCategory $category
 * @property User $user
 * @property string $title
 * @property string $slug
 * @property string $content_raw
 * @property string $content_html
 * @property string $excerpt
 * @property bool $is_published
 * @property string $published_at
 */
class BlogPost extends Model
{
    use SoftDeletes;

    const UNKNOWN_USER = 1;

    protected $fillable = [
        'title',
        'slug',
        'category_id',
        'excerpt',
        'content_raw',
        'is_published',
        'published_at',
    ];

	/**
	 * Категорія статті.
	 *
	 * @return BelongsTo
	 */
    public function category()
    {
    	// Стаття належить категорії
    	return $this->belongsTo(BlogCategory::class);
    }

	/**
	 * Автор статті.
	 *
	 * @return BelongsTo
	 */
    public function user()
    {
    	// Стаття належить користувачу
    	return $this->belongsTo(User::class);
    }
}
