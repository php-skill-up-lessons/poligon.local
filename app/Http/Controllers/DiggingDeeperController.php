<?php

namespace App\Http\Controllers;

use App\Jobs\GenerateCatalog\GenerateCatalogMainJob;
use App\Jobs\ProcessVideoJob;
use App\Models\BlogPost;
use Illuminate\Support\Carbon;

class DiggingDeeperController extends Controller
{
	/**
	 * Базова інформація:
	 * @url https://laravel.com/docs/5.8/collections
	 *
	 * Довідкова інформація:
	 * @url https://laravel.com/api/5.8/Illuminate/Support/Collection.html
	 *
	 * Варіант колекції для моделей eloquent:
	 * @url https://laravel.com/api/5.8/Illuminate/Database/Eloquent/Collection.html
	 *
	 * Білдер запитів (те з чим можливо сплутати колекції):
	 * @url https://laravel.com/docs/5.8/queries	 *
	 */
    public function collections()
	{
		$result = [];

		/**
		 * @var \Illuminate\Database\Eloquent\Collection $eloquentCollection
		 */
		$eloquentCollection = BlogPost::withTrashed()->get();

//		dd(__METHOD__, $eloquentCollection, $eloquentCollection->toArray());

		/**
		 * @var \Illuminate\Support\Collection $collection
		 */
		$collection = collect($eloquentCollection->toArray());

//		dd(__METHOD__,
//			get_class($eloquentCollection),
//			get_class($collection),
//			$collection
//		);

		$result['first'] = $collection->first();
		$result['last'] = $collection->last();

		$result['where']['data'] = $collection
			->where('category_id', '=', 10)
			->values()
			->keyBy('id')
		;

		$result['where']['count'] = $result['where']['data']->count();
		$result['where']['isEmpty'] = $result['where']['data']->isEmpty();
		$result['where']['isNotEmpty'] = $result['where']['data']->isNotEmpty();

		// Не дуже гарно
		if ($result['where']['count']) {
			//...
		}
		// Так краще
		if ($result['where']['data']->isEmpty()) {
			//...
		}

		$result['where_first'] = $collection
			->firstWhere('created_at', '>', '2020-09-10 06:00:38');

		// Базова змінна не змінюється. Просто повернути змінену версію
		$result['map']['all'] = $collection->map(function (array $item) {
			$newItem = new \stdClass();
			$newItem->item_id = $item['id'];
			$newItem->item_name = $item['title'];
			$newItem->exists = is_null($item['deleted_at']);

			return $newItem;
		});

		$result['map']['not_exist'] = $result['map']['all']
			->where('exists', '=', false)
			->values()
			->keyBy('item_id')
		;

//		dd($result);

		// Базова змінна змінюється (трансформується).
		$collection->transform(function (array $item) {
			$newItem = new \stdClass();
			$newItem->item_id = $item['id'];
			$newItem->item_name = $item['title'];
			$newItem->exists = is_null($item['deleted_at']);
			$newItem->created_at = Carbon::parse($item['created_at']);

			return $newItem;
		});

//		$newItem = new \stdClass();
//		$newItem->id = 9999;
//
//		$newItem2 = new \stdClass();
//		$newItem2->id = 8888;
//
//		// Встановити елемент в початок колекції
//		$newItemFirst = $collection->prepend($newItem)->first();
//		$newItemLast = $collection->push($newItem2)->last();
//		$pulledItem = $collection->pull(10);
//
//		dd(compact('collection', 'newItemFirst', 'newItemLast', 'pulledItem'));

//		// Фільтрація. Заміна orWhere()
//		$filtered = $collection->filter(function ($item) {
////			return $item->created_at->isFriday() && ($item->created_at->day == 11);
//			$byDay = $item->created_at->isFriday();
//			$byDate = $item->created_at->day == 11;
//
//			return $byDay && $byDate;
//		});
//		dd($filtered);

		$sortedSimpleCollection = collect([5, 3, 1, 2, 4])->sort();
		$sortedAscCollection = $collection->sortBy('created_at');
		$sortedDescCollection = $collection->sortByDesc('item_id');

		dd(compact('sortedSimpleCollection', 'sortedAscCollection', 'sortedDescCollection'));
	}

	public function processVideo()
	{
		ProcessVideoJob::dispatch()
			// Відстрочка виконання завдання від момента розміщення в чергу.
			// Не впливає на паузу між спробами виконати завдання.
			//->delay(10)
			//->onQueue('name_of_queue)
		;
	}

	/**
	 * @link http://poligon.local/digging_depper/
	 *
	 * php artisan queue:listen --queue=generate-catalog --tries=3 --delay=10
	 */
	public function prepareCatalog()
	{
		GenerateCatalogMainJob::dispatch();
	}
}
