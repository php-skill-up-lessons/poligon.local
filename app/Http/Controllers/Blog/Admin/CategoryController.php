<?php

namespace App\Http\Controllers\Blog\Admin;

use App\Http\Requests\BlogCategoryCreateRequest;
use App\Http\Requests\BlogCategoryUpdateRequest;
use App\Models\BlogCategory;
use App\Repositories\BlogCategoryRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Управління категоріями блогу в адмінці
 *
 * Class CategoryController
 *
 * @package App\Http\Controllers\Blog\Admin
 */
class CategoryController extends BaseController
{
	/**
	 * @var BlogCategoryRepository
	 */
	private $blogCategoryRepository;

	public function __construct()
	{
		parent::__construct();

		$this->blogCategoryRepository = app(BlogCategoryRepository::class);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index(): View
    {
		$paginator = $this->blogCategoryRepository->getAllWithPaginate(25);

		return view('blog.admin.categories.index', compact('paginator'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return View
	 */
	public function create(): View
    {
		$item = BlogCategory::make();
		$categoryList = $this->blogCategoryRepository->getForComboBox();

		return view('blog.admin.categories.edit', compact('item', 'categoryList'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param BlogCategoryCreateRequest $request
	 *
	 * @return RedirectResponse
	 */
	public function store(BlogCategoryCreateRequest $request): RedirectResponse
    {
		$data = $request->input();
		// Створити обʼєкт та додати в БД
		$item = BlogCategory::create($data);

		if ($item->exists) {
			return redirect()->route('blog.admin.categories.edit', [$item->id])
			                 ->with(['success' => 'Успішно збережено']);
		} else {
			return back()->withErrors(['msg' => 'Помилка збереження'])
			             ->withInput();
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return View
	 */
	public function edit(int $id): View
    {
		$item = $this->blogCategoryRepository->getEdit($id);

		if (empty($item)) {
			abort(404);
		}

		$categoryList = $this->blogCategoryRepository->getForComboBox();

		return view('blog.admin.categories.edit',
			compact('item', 'categoryList'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param BlogCategoryUpdateRequest $request
	 * @param int $id
	 *
	 * @return RedirectResponse
	 */
	public function update(BlogCategoryUpdateRequest $request, int $id): RedirectResponse
    {
		$item = $this->blogCategoryRepository->getEdit($id);

		if (empty($item)) {
			return back()
				->withErrors(['msg' => "Запис id=[{$id}] не знайдений!"])
				->withInput();
		}

		$data = $request->all();
		$result = $item->update($data);

		if ($result) {
			return redirect()
				->route('blog.admin.categories.edit', $item->id)
				->with(['success' => 'Успішно збережено!']);
		} else {
			return back()
				->withErrors(['msg' => 'Помилка збереження!'])
				->withInput();
		}
	}
}
