<?php

namespace App\Http\Controllers\Blog\Admin;

use App\Http\Requests\BlogPostCreateRequest;
use App\Http\Requests\BlogPostUpdateRequest;
use App\Jobs\BlogPostAfterCreateJob;
use App\Jobs\BlogPostAfterDeleteJob;
use App\Models\BlogPost;
use App\Repositories\BlogCategoryRepository;
use App\Repositories\BlogPostRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;

/**
 * Управління статями блога
 *
 * Class PostController
 *
 * @package App\Http\Controllers\Blog\Admin
 */
class PostController extends BaseController
{
	/**
	 * @var BlogPostRepository
	 */
	private $blogPostRepository;

	/**
	 * @var BlogCategoryRepository
	 */
	private $blogCategoryRepository;

	/**
	 * PostController constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->blogPostRepository = app(BlogPostRepository::class);
		$this->blogCategoryRepository = app(BlogCategoryRepository::class);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
    public function index(): View
    {
	    $paginator = $this->blogPostRepository->getAllWithPaginate();

        return view('blog.admin.posts.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
	{
        $item = new BlogPost();
        $categoryList = $this->blogCategoryRepository->getForComboBox();

        return view('blog.admin.posts.edit', compact('item', 'categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BlogPostCreateRequest $request
     *
     * @return RedirectResponse
	 */
    public function store(BlogPostCreateRequest $request): RedirectResponse
	{
		$data = $request->input();
		$item = BlogPost::create($data);

		if ($item) {
			$job = new BlogPostAfterCreateJob($item);
			$this->dispatch($job);

			return redirect()->route('blog.admin.posts.edit', [$item->id])
							 ->with(['success' => 'Успішно збережено']);
		} else {
			return back()->withErrors(['msg' => 'Помилка збереження'])
						 ->withInput();
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return View
     */
    public function edit(int $id): View
    {
        $item = $this->blogPostRepository->getEdit($id);

        if (empty($item)) {
            abort(404);
        }

        $categoryList = $this->blogCategoryRepository->getForComboBox();

        return view('blog.admin.posts.edit',
            compact('item', 'categoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BlogPostUpdateRequest $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function update(BlogPostUpdateRequest $request, int $id): RedirectResponse
    {
        $item = $this->blogPostRepository->getEdit($id);

        if (empty($item)) {
            return back()
                ->withErrors(['msg' => "Запис id=[{$id}] не знайдений"])
                ->withInput();
        }

        $data = $request->all();
        $request = $item->update($data);

        if ($request) {
            return redirect()
                ->route('blog.admin.posts.edit', $item->id)
                ->with(['success' => 'Збережено вдало']);
        } else {
            return back()
                ->withErrors(['msg' => 'Помилка збереження'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
	 * @return RedirectResponse
	 */
    public function destroy(int $id): RedirectResponse
	{
    	// Софт видалення, в БД залишиться
	    $result = BlogPost::destroy($id);

	    // Повне видалення
		//$result = BlogPost::find($id)->forceDelete();

	    if ($result) {
	    	BlogPostAfterDeleteJob::dispatch($id)->delay(20);

	    	//> Варіанти запуска:
	    	//BlogPostAfterDeleteJob::dispatchNow();

			//dispatch(new BlogPostAfterDeleteJob($id));
			//dispatch_now(new BlogPostAfterDeleteJob($id));

			//$this->dispatch(new BlogPostAfterDeleteJob($id));
			//$this->dispatchNow(new BlogPostAfterDeleteJob($id));

			/*$job = new BlogPostAfterDeleteJob($id);
			$job->handle();*/
			//< Варіанти запуска.

			$restoreLink = '<a href="'. route('blog.admin.posts.restore', $id) . '">посилання</a>';

	    	return redirect()
				->route('blog.admin.posts.index')
				->with(['success' => "Запис id[$id] видалений.\n<br>" .
									 "Для відновлення запису id[$id] натисніть на $restoreLink."]);
		} else {
	    	return back()->withErrors(['msg' => 'Помилка видалення']);
		}
	}

	/**
	 * Restore the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return RedirectResponse
	 */
	public function restore(int $id): RedirectResponse
	{
		$blogPost = BlogPost::onlyTrashed()->find($id);

		if ($blogPost) {
			$blogPost->restore();
			return redirect()
				->route('blog.admin.posts.edit', $id)
				->with(['success' => "Запис id[$id] відновлений"]);
		} else {
			return back()->withErrors(['msg' => "Неможливо відновити запис id[$id]"]);
		}
    }
}
