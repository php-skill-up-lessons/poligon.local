<?php

namespace App\Http\Controllers\Blog\Admin;

use App\Http\Controllers\Blog\BaseController as GuestBaseController;

/**
 * Class BaseController
 *
 * @package App\Http\Controllers\Blog\Admin
 *
 * Базовий контролер для всіх контролерів управління
 * блогом в панелі адміністрування.
 * Повинен буди батьком усіх контролерів управління блогом.
 */
abstract class BaseController extends GuestBaseController
{
	/**
	 * BaseController constructor.
	 */
	public function __construct()
	{
		// Ініціалізація спільних моментів для адмінки.
	}

}
