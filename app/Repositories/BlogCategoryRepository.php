<?php

namespace App\Repositories;

use App\Models\BlogCategory as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BlogCategoryRepository
 *
 * @package App\Repositories
 */
class BlogCategoryRepository extends CoreRepository
{
	/**
	 * @return string
	 */
	protected function getModelClass()
	{
		return Model::class;
	}

	/**
	 * Отримати модель для редагування в адмінці
	 *
	 * @param int $id
	 *
	 * @return Model
	 */
	public function getEdit(int $id)
	{
		return $this->startConditions()->find($id);
	}

	/**
	 * Отримати список категорій для виведення в випадаючому списку.
	 *
	 * @return Collection
	 */
	public function getForComboBox()
	{
		$columns = implode(', ', [
			'id',
			'CONCAT (id, ". ", title) AS id_title',
		]);

		return $this
			->startConditions()
			->selectRaw($columns)
			->toBase()
			->get();
	}

	/**
	 * Отримати категорії для вивода пагінатором.
	 *
	 * @param int|null $perPage
	 *
	 * @return LengthAwarePaginator
	 */
	public function getAllWithPaginate($perPage = null)
	{
		$columns = ['id', 'title', 'parent_id'];

		return $this
			->startConditions()
			->select($columns)
			->with([
				'parentCategory:id,title',
			])
			->paginate($perPage);
	}
}
