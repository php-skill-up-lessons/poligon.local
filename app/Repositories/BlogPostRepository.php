<?php

namespace App\Repositories;

use App\Models\BlogPost as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class BlogCategoryRepository
 *
 * @package App\Repositories
 */
class BlogPostRepository extends CoreRepository
{
	/**
	 * @return string
	 */
	protected function getModelClass()
	{
		return Model::class;
	}

	/**
	 * Отримати список статей для виведення в списку
	 * в адмінці
	 *
	 * @return LengthAwarePaginator
	 */
	public function getAllWithPaginate()
	{
		$columns = [
			'id',
			'title',
			'slug',
			'is_published',
			'published_at',
			'user_id',
			'category_id',
		];

		return $this->startConditions()
					->select($columns)
					->orderBy('id', 'DESC')
					->with([
						'category' => function($query) {
							$query->select(['id', 'title']);
						},
						'user:id,name',
					])
					->paginate(25);
	}

    /**
     * Отримати модель для редагування в адмінці
     *
     * @param int $id
     *
     * @return Model
     */
	public function getEdit(int $id)
    {
        return $this->startConditions()->find($id);
    }
}
