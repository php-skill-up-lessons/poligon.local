<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Application;

/**
 * Class CoreRepository
 *
 * @package App\Repositories
 *
 * Репозиторій роботи с обʼєктом.
 * Може видавати набори даних.
 * Не може створювати/змінювати обʼєкти.
 */
abstract class CoreRepository
{
	/**
	 * @var Model
	 */
	protected $model;

	/**
	 * CoreRepository constructor.
	 */
	public function __construct()
	{
		$this->model = app($this->getModelClass());
	}

	/**
	 * @return string
	 */
	abstract protected function getModelClass();

	/**
	 * @return Model|Application|mixed
	 */
	protected function startConditions()
	{
		return clone $this->model;
	}
}