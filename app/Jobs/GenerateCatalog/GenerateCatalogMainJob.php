<?php

namespace App\Jobs\GenerateCatalog;

class GenerateCatalogMainJob extends AbstractJob
{
    /**
	 * @throw \Psr\SimpleCache\InvalidArgumentException
	 * @throw \Throwable
     */
    public function handle()
    {
        $this->debug('start');

        // Спочатку кешуємо продукти
		GenerateCatalogCacheJob::dispatchNow();

		// Потім створюємо цепочку завдань формування файлів з цінами
		$chainPrices = $this->getChainPrices();

		// Основні підзадачі
		$chainMain = [
			new GenerateCategoriesJob(), // Генерація категорій
			new GenerateDeliveriesJob(), // Генерація способів доставки
			new GeneratePointJob(), // Генерація пунктів видачі
		];

		// Підзадачі які повинні виконуватися останніми
		$chainLast = [
			// Архівування файлів та перенос архіва в публічну папку
			new ArchiveUploadsJob(),
			// Відправка повідомлень сторонньому сервісу про те що можно завантажити новий файл каталога товарів
			new SendPriceRequestJob(),
		];

		$chain = array_merge($chainPrices, $chainMain, $chainLast);

		GenerateGoodsFileJob::withChain($chain)->dispatch();
		//GenerateGoodsFileJob::dispatch()->chain($chain);

		$this->debug('finish');
    }

	/**
	 * Формування цепочок підзадач по генерації файлів з цінами
	 *
	 * @return array
	 */
    private function getChainPrices(): array
	{
		$result = [];
		$products = collect([1, 2, 3, 4, 5]);
		$fileNumber = 1;

		foreach ($products->chunk(1) as $chunk) {
			$result[] = new GeneratePricesFileChunkJob($chunk, $fileNumber);
			$fileNumber++;
		}

		return $result;
	}
}
