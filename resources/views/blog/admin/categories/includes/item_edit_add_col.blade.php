<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <button type="submit" class="btn btn-primary">Зберегти</button>
            </div>
        </div>
    </div>
</div>
<br>
@php /** @var App\Models\BlogCategory $item */ @endphp
@if($item->exists)
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <ul class="list-unstyled">
                        <li>ID: {{ $item->id }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="created_at">Створено</label>
                        <input type="text" value="{{ $item->created_at }}" id="created_at" class="form-control" disabled>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Змінено</label>
                        <input type="text" value="{{ $item->updated_at }}" id="updated_at" class="form-control" disabled>
                    </div>
                    <div class="form-group">
                        <label for="deleted_at">Видалено</label>
                        <input type="text" value="{{ $item->deleted_at }}" id="deleted_at" class="form-control" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
