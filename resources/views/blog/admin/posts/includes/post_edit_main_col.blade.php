@php /** @var App\Models\BlogPost $item */ @endphp
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @if($item->is_published)
                    Опубліковано
                @else
                    Чернетка
                @endif
            </div>
            <div class="card-body">
                <div class="card-title">
                </div>
                <div class="card-subtitle mb-2 text-muted">
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a href="#main-data" class="nav-link active" data-toggle="tab" role="tab">
                            Основні дані
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#add-data" class="nav-link" data-toggle="tab" role="tab">
                            Додаткові дані
                        </a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div id="main-data" class="tab-pane active" role="tabpanel">
                        <div class="form-group">
                            <label for="title">Заголовок</label>
                            <input name="title" type="text" value="{{ old('title', $item->title) }}"
                                   id="title" class="form-control" minlength="3" required>
                        </div>
                        <div class="form-group">
                            <label for="content_row">Стаття</label>
                            <textarea name="content_raw" id="content_row" class="form-control" rows="20">{{ old('content_raw', $item->content_raw) }}</textarea>
                        </div>
                    </div>
                    <div id="add-data" class="tab-pane" role="tabpanel">
                        <div class="form-group">
                            <label for="category_id">Категорія</label>
                            <select name="category_id" id="category_id" class="form-control"
                                    placeholder="Виберіть категорію" required>
                                @php
                                    /** @var Illuminate\Support\Collection $categoryList */
                                    /** @var App\Models\BlogCategory $categoryOption */
                                @endphp
                                @foreach($categoryList as $categoryOption)
                                    <option value="{{ $categoryOption->id }}"
                                            @if($categoryOption->id == old('category_id', $item->category_id)) selected @endif>
                                        {{ $categoryOption->id_title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="slug">Ідентифікатор</label>
                            <input name="slug" type="text" value="{{ old('slug', $item->slug) }}"
                                   id="slug" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="excerpt">Витримка</label>
                            <textarea name="excerpt" id="excerpt" class="form-control" rows="3">
{{ old('excerpt', $item->excerpt) }}
                            </textarea>
                        </div>
                        <div class="form-check">
                            <input name="is_published" type="hidden" value="0">
                            <input name="is_published" type="checkbox" id="is_published" class="form-check-input"
                                   value="1"
                                   @if($item->is_published) checked @endif>
                            <label for="is_published" class="form-check-label">Опубліковано</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
